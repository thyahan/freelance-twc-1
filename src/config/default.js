module.exports = {
  host: process.env.NODE_HOST || 'localhost', // Define your host from 'package.json'
  port: process.env.PORT,
  app: {
    htmlAttributes: { lang: 'en' },
    title: 'ivyflow React Starter',
    titleTemplate: 'ivyflow React Starter - %s',
    meta: [
      {
        name: 'description',
        content: 'ivyflow react universal starter.',
      },
    ],
  },
};
